import React, { Component } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Home } from "./Screen/Home";
import { Login } from "./Screen/Login";
import { Register } from "./Screen/Register";
import { PageNotFound } from "./Screen/PageNotFound";
import { NavigationBar } from "./Components/NavigationBar";
import { Jumbotron } from "./Components/Jumbotron";
import { NavigationBarTop } from "./Components/NavigationBarTop";
import { Footer } from "./Components/Footer";
class App extends Component {
  render() {
    return (
      <div
        style={{
          backgroundColor: "#fff",
          // backgroundColor: "#052043",
        }}
      >
        <BrowserRouter>
          <NavigationBarTop />
          <NavigationBar />
          <Jumbotron />
          <Routes>
            <Route exact path="/" element={<Home />}></Route>
            <Route path="/login" element={<Login />}></Route>
            <Route path="/register" element={<Register />}></Route>
            <Route element={<PageNotFound />}></Route>
          </Routes>
          <Footer />
        </BrowserRouter>
      </div>
    );
  }
}

export default App;

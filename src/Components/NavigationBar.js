import React from "react";
import { Image } from "react-bootstrap";
import styled from "styled-components";
import { Calendar, Logo } from "../assets";

const Styles = styled.div`
  .navbar {
    background-color: #fff;
  }

  a,
  .navbar-brand,
  .navbar-nav .nav-link {
    color: #000;

    &:hover {
      color: #000;
    }
  }
`;

export const NavigationBar = () => (
  <Styles>
    <nav className="navbar navbar-expand-lg navbar-light">
      <div className="container">
        <a class="navbar-brand" href="#">
          <img src={Logo} alt="" width="80" height="40" />
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav ms-auto">
            <li className="nav-item me-4">
              <a className="nav-link active" aria-current="page" href="#">
                Paket Test Darah
              </a>
            </li>
            <li className="nav-item me-4">
              <a className="nav-link" href="#">
                Laboratorium
              </a>
            </li>
            <li className="nav-item me-4">
              <a className="nav-link" href="#">
                Artikel
              </a>
            </li>
            <li className="nav-item me-4">
              <a
                className="btn mb-2"
                style={{ color: "#305DC3", backgroundColor: "#EBF4FE" }}
                href="/login"
              >
                Masuk
              </a>
            </li>
            <li className="nav-item">
              <a
                className="btn btn-primary fw-bold"
                style={{ color: "#fff" }}
                href="/register"
              >
                Daftar
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </Styles>
);

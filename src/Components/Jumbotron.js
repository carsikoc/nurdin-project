import React from "react";
import { Jumbotron as Jumbo, Container } from "react-bootstrap";
import styled from "styled-components";
// import boatImage from "../assets/boatImage.jpg";
import { Jumbotrons } from "../assets";

const Styles = styled.div`
  .jumbo {
    margin-left: -10px !important;
    margin-right: -30px !important;
  }
`;

export const Jumbotron = () => (
  <Styles>
    <div fluid className>
      <img
        src={Jumbotrons}
        alt=""
        className="img-thumbnail jumbo"
        style={{ resize: "cover" }}
      />
    </div>
  </Styles>
);

import React from "react";
import styled from "styled-components";
import { Calendar, Telp, Email } from "../assets";

const Styles = styled.div`
  // Large devices (desktops, 992px and up)
  @media (min-width: 992px) {
    .navbar {
      height: 40px;
    }
  }
  .navbar {
    height: 40px;
  }
`;

export const NavigationBarTop = () => (
  <Styles>
    <div
      className="navbar d-flex justify-content-end pe-5"
      style={{
        backgroundColor: "#F4F6F7",
        alignItems: "center",
      }}
    >
      <div className="col-2 my-1">
        <img src={Calendar} className="float-start mx-2" alt=""></img>
        <p style={{ fontSize: 12, color: "#464646" }} className="fw-100">
          Senin - Jumat (09.00 - 18.00)
        </p>
      </div>
      <div className="col-2 my-1">
        <img src={Telp} className="float-start mx-2" alt=""></img>

        <p style={{ fontSize: 12, color: "#464646" }}>+628111122015</p>
      </div>
      <div className="col-2 my-1">
        <img src={Email} className="float-start mx-2" alt=""></img>
        <p style={{ fontSize: 12, color: "#464646" }}>cs@triasse.com</p>
      </div>
    </div>
  </Styles>
);

import React from "react";
import {
  TriasseTransparent,
  Facebook,
  Linkedin,
  Instagram,
  Pinteres,
  Twitter,
  Youtube,
} from "../assets";

export const Footer = () => {
  return (
    <div>
      <div
        className="row justify-content-center "
        style={{
          backgroundColor: "#218AE5",
          height: 150,
          alignItems: "center",
        }}
      >
        <div className="col-sm-8">
          <div className="row">
            <div className="col-sm-7">
              <p
                className="fw-bold pt-2"
                style={{ fontSize: 24, color: "#fff" }}
              >
                Berlangganan newsletter spesial promo dan penawan menarik
              </p>
            </div>
            <div className="col-sm-5 ">
              <div className="row " style={{ height: 50 }}>
                <div
                  className="col-sm-7 text-center "
                  style={{ backgroundColor: "#fff" }}
                >
                  <p className="pt-3" style={{ color: "#464646" }}>
                    Masukkan Email Anda
                  </p>
                </div>
                <div
                  className="col-sm-5 text-center "
                  style={{ backgroundColor: "#FE7A19" }}
                >
                  <p className="pt-3 fw-bold" style={{ color: "#fff" }}>
                    Berlangganan
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className="row justify-content-center py-5"
        style={{
          backgroundColor: "#0B315F",
          alignItems: "center",
        }}
      >
        <div className="col-sm-10">
          <div className="row">
            <div className="col-sm-3">
              <img src={TriasseTransparent} alt=""></img>
              <p className="text-light mt-3">
                Triasse adalah sebuah platform yang menyatukan berbagai
                laboratorium terbesar dan terpercaya di Indonesia dalam satu
                wadah. Hal ini memungkinkan konsumen dan klien untuk mengecek
                berbagai pemeriksaan kesehatan, tes darah, rapid test dan
                lainnya hanya dari satu situs.
              </p>
            </div>
            <div className="col-sm-2">
              <h5 className="text-light fw-bold mb-5">Lainnya</h5>
              <p className="text-light">Syarat dan Ketentuan</p>
              <p className="text-light">Karier</p>
              <p className="text-light">Kebijakan Privasi</p>
              <p className="text-light">Cara Kerja</p>
              <p className="text-light">Login</p>
            </div>
            <div className="col-sm-2">
              <h5 className="text-light fw-bold mb-5">Hubungi Kami</h5>
              <p className="text-light">cs@triasse.com</p>
              <p className="text-light">021-35760497</p>
              <p className="text-light">08-1111-22-015</p>
              <p className="text-light">Senin - Jumat</p>
              <p className="text-light">08.00 - 18.00 WIB</p>
            </div>
            <div className="col-sm-3">
              <h5 className="text-light fw-bold mb-5">Alamat</h5>
              <p className="text-light">Gedung Perkantoran Madison Parkm</p>
              <p className="text-light">
                Lantai 6 unit B, No. 02. Jl. Letjen S. Parman
              </p>
              <p className="text-light">Kav 28, Jakarta 11470</p>
            </div>
            <div className="col-sm-2">
              <h5 className="text-light fw-bold mb-5">Media Sosial</h5>
              <div className="row">
                <div className="col-4">
                  <img src={Facebook} alt="" style={{ height: 50 }} />
                </div>
                <div className="col-4">
                  <img src={Instagram} alt="" style={{ height: 50 }} />
                </div>
                <div className="col-4">
                  <img src={Linkedin} alt="" style={{ height: 50 }} />
                </div>
              </div>
              <div className="row mt-4">
                <div className="col-4">
                  <img src={Pinteres} alt="" style={{ height: 50 }} />
                </div>
                <div className="col-4">
                  <img src={Twitter} alt="" style={{ height: 50 }} />
                </div>
                <div className="col-4">
                  <img src={Youtube} alt="" style={{ height: 50 }} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className="row text-center align-items-center"
        style={{ backgroundColor: "#052043", height: 80 }}
      >
        <p className="text-light">
          Copyright 2022 PT Digital Medika Informatika. All rights reserved
        </p>
      </div>
    </div>
  );
};

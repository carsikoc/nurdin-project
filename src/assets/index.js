import Logo from "./img/img-brand-logo.png";
import Calendar from "./img/img-calendar.svg";
import Telp from "./img/img-message.svg";
import Email from "./img/img-email.svg";
import Jumbotrons from "./img/img-big-banner@2x.jpg";
import Doktor from "./img/img-doctor.svg";
import Medical from "./img/medical.jpg";
import Banner_1 from "./img/img-banner2-1.jpg";
import Banner_2 from "./img/img-banner2-2.jpg";
import Example_1 from "./img/img-example-1.png";
import Example_2 from "./img/img-example-2.png";
import Example_3 from "./img/img-example-3.png";
import Prodia from "./img/img-mitra-1.png";
import Prahita from "./img/img-mitra-2.png";
import KimiaFarma from "./img/img-mitra-3.png";
import Biotest from "./img/img-mitra-4.png";
import GSLab from "./img/img-mitra-5.png";
import ImagePost_1 from "./img/img-post-1.jpg";
import Media_1 from "./img/img-media-coverage-1.png";
import Media_2 from "./img/img-media-coverage-2.png";
import Media_3 from "./img/img-media-coverage-3.png";
import Media_4 from "./img/img-media-coverage-4.png";
import Media_5 from "./img/img-media-coverage-5.png";
import Media_6 from "./img/img-media-coverage-6.png";
import Media_7 from "./img/img-media-coverage-7.png";
import Media_8 from "./img/img-media-coverage-8.png";
import Media_9 from "./img/img-media-coverage-9.png";
import Media_10 from "./img/img-media-coverage-10.png";
import Media_11 from "./img/img-media-coverage-11.png";
import Media_12 from "./img/img-media-coverage-12.png";
import TriasseTransparent from "./img/img-brand-logo-white@2x.png";
import Facebook from "./img/img-icon-facebook.svg";
import Linkedin from "./img/img-icon-linkedin.svg";
import Instagram from "./img/img-icon-instagram.svg";
import Pinteres from "./img/img-icon-pinteres.svg";
import Twitter from "./img/img-icon-twitter.svg";
import Youtube from "./img/img-icon-youtube.svg";

export {
  ImagePost_1,
  Logo,
  Calendar,
  Telp,
  Email,
  Jumbotrons,
  Doktor,
  Medical,
  Banner_1,
  Banner_2,
  Example_1,
  Example_2,
  Example_3,
  Prodia,
  Prahita,
  KimiaFarma,
  Biotest,
  GSLab,
  Media_1,
  Media_2,
  Media_3,
  Media_4,
  Media_5,
  Media_6,
  Media_7,
  Media_8,
  Media_9,
  Media_10,
  Media_11,
  Media_12,
  TriasseTransparent,
  Facebook,
  Linkedin,
  Instagram,
  Pinteres,
  Twitter,
  Youtube,
};

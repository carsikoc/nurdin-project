import React from "react";
import { Button, Card } from "react-bootstrap";
import {
  Doktor,
  Medical,
  Banner_1,
  Banner_2,
  Example_1,
  Example_2,
  Example_3,
  Prodia,
  Prahita,
  KimiaFarma,
  Biotest,
  GSLab,
  ImagePost_1,
  Media_1,
  Media_2,
  Media_3,
  Media_4,
  Media_5,
  Media_6,
  Media_7,
  Media_8,
  Media_9,
  Media_10,
  Media_11,
  Media_12,
} from "../assets";

export const Home = () => {
  return (
    <div>
      <div
        className="row-10 shadow-lg rounded-3 container"
        style={{
          marginTop: -100,
          backgroundColor: "#fff",
          position: "relative",
          zIndex: 0,
        }}
      >
        <div className="col rounded-3">
          <div
            className="row justify-content-around pt-3 rounded-3"
            style={{ backgroundColor: "#EFF7FF" }}
          >
            <div className="col-5 text-start">
              <div className="row">
                <h4 style={{ color: "#0D1D4A" }}>Pilih jenis pemeriksaan</h4>
              </div>
              <div className="row">
                <h6 style={{ fontWeight: 300 }}>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry
                </h6>
              </div>
            </div>
            <div className="col-5 text-center">
              <img
                src={Doktor}
                alt=""
                className="img"
                height={100}
                // width={100}
              />
            </div>
          </div>
          <div
            className="row justify-content-center py-5"
            style={{
              backgroundColor: "#fff",
              paddingLeft: 40,
              paddingRight: 40,
            }}
          >
            <div class="form-group col-4">
              <label for="inputCity">Province</label>
              <select id="inputState" class="form-control">
                <option selected>Province</option>
                <option>...</option>
              </select>
            </div>
            <div class="form-group col-4">
              <label for="inputState">Kabupaten/Kota</label>
              <select id="inputState" class="form-control">
                <option selected>Kabupaten/kota</option>
                <option>...</option>
              </select>
            </div>
            <div class="form-group col-4">
              <label for="inputZip">Paket & jenis pemeriksaan</label>
              <select id="inputState" class="form-control">
                <option selected>Pilih paket & jenis pemeriksaan</option>
                <option>...</option>
              </select>
            </div>
          </div>
          <div className="row justify-content-end">
            <a
              href=""
              className="btn col-1 fw-bold text-light mx-5 mb-5"
              style={{ backgroundColor: "#FE7A19" }}
            >
              Cari Lab
            </a>
          </div>
        </div>
      </div>
      <div
        className="row py-5"
        style={{ backgroundColor: "#F6FBFF", marginTop: 100 }}
      >
        <div className="row">
          <div className="col text-center">
            <h3 style={{ color: "#162552" }}>Mengapa Harus Triasse ?</h3>
            <h6 style={{ color: "#162552" }}>
              Lorem Ipsum has been the industry's standard dummy text ever since
              the 1500s
            </h6>
          </div>
        </div>
        <div className="row justify-content-center mt-5">
          <div
            className="col-lg-4 col-md-4 col-sm-4 mx-3 rounded-3"
            style={{ backgroundColor: "#EBEBEB" }}
          ></div>
          <div className="col-lg-4 col-md-4 col-sm-4 mx-3">
            <div className="row mb-4">
              <div
                className="col-sm-3 rounded-2"
                style={{ backgroundColor: "#EBEBEB" }}
              ></div>
              <div className="col-sm-8">
                <div className="row">
                  <h6 className="fw-bold" style={{ color: "#162552" }}>
                    Layanan Home Service
                  </h6>
                  <h6 className="fw-light" style={{ color: "#162552" }}>
                    Harga kompetitif di Laboratium terbesar dan terpercaya di
                    indonesia seperti Prodia, Kimia FarmaParahita, GS Lab &
                    Biotest.
                  </h6>
                </div>
              </div>
            </div>
            <div className="row mb-4">
              <div
                className="col-sm-3 rounded-2"
                style={{ backgroundColor: "#EBEBEB" }}
              ></div>
              <div className="col-sm-8">
                <div className="row">
                  <h6 className="fw-bold" style={{ color: "#162552" }}>
                    Cicilan 4x dengan bunga 4%
                  </h6>
                  <h6 className="fw-light" style={{ color: "#162552" }}>
                    Harga kompetitif di Laboratium terbesar dan terpercaya di
                    indonesia seperti Prodia, Kimia FarmaParahita, GS Lab &
                    Biotest
                  </h6>
                </div>
              </div>
            </div>
            <div className="row mb-4">
              <div
                className="col-sm-3 rounded-2"
                style={{ backgroundColor: "#EBEBEB" }}
              ></div>
              <div className="col-sm-8">
                <div className="row">
                  <h6 className="fw-bold" style={{ color: "#162552" }}>
                    Pesan dan hasil online
                  </h6>
                  <h6 className="fw-light" style={{ color: "#162552" }}>
                    Harga kompetitif di Laboratium terbesar dan terpercaya di
                    indonesia seperti Prodia, Kimia FarmaParahita, GS Lab &
                    Biotest
                  </h6>
                </div>
              </div>
            </div>
            <div className="row mb-4">
              <div
                className="col-sm-3 rounded-2"
                style={{ backgroundColor: "#EBEBEB" }}
              ></div>
              <div className="col-sm-8">
                <div className="row">
                  <h6 className="fw-bold" style={{ color: "#162552" }}>
                    Jaminan Harga Terbaik
                  </h6>
                  <h6 className="fw-light" style={{ color: "#162552" }}>
                    Harga kompetitif di Laboratium terbesar dan terpercaya di
                    indonesia seperti Prodia, Kimia FarmaParahita, GS Lab &
                    Biotest
                  </h6>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container" style={{ marginTop: 100 }}>
        <div className="row">
          <div className="col text-center mx-2">
            <h3 style={{ color: "#162552" }}>
              Lorem Ipsum has been the industry's
            </h3>
            <h5 style={{ color: "#162552", marginTop: 30 }}>
              Lorem Ipsum has been the industry's Lorem Ipsum has been the
              industry's standard dummy text ever since the 1500s
            </h5>
          </div>
        </div>
        <div className="row my-5 justify-content-around ">
          <Card className="rounded-3 shadow-lg" style={{ width: "12rem" }}>
            <Card.Img
              variant="top"
              src={Medical}
              style={{ resizeMode: "cover" }}
            />
            <Card.Body>
              <Card.Title style={{ color: "#162552", fontSize: 18 }}>
                Paket Medical Check - up Basic
              </Card.Title>
              <Card.Text style={{ color: "#464646", fontWeight: "500" }}>
                Harga mulai dari
              </Card.Text>
              <Card.Text
                className="text-decoration-line-through"
                style={{ color: "#464646", fontWeight: "500" }}
              >
                Rp. 364.091
              </Card.Text>
              <Card.Text style={{ color: "#162552", fontWeight: "700" }}>
                Rp. 364.091
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className="rounded-3 shadow-lg" style={{ width: "12rem" }}>
            <Card.Img
              variant="top"
              src={Medical}
              style={{ resizeMode: "cover" }}
            />
            <Card.Body>
              <Card.Title style={{ color: "#162552", fontSize: 18 }}>
                Paket Medical Check - up Silver
              </Card.Title>
              <Card.Text style={{ color: "#464646", fontWeight: "500" }}>
                Harga mulai dari
              </Card.Text>
              <Card.Text
                className="text-decoration-line-through"
                style={{ color: "#464646", fontWeight: "500" }}
              >
                Rp. 364.091
              </Card.Text>
              <Card.Text style={{ color: "#162552", fontWeight: "700" }}>
                Rp. 364.091
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className="rounded-3 shadow-lg" style={{ width: "12rem" }}>
            <Card.Img
              variant="top"
              src={Medical}
              style={{ resizeMode: "cover" }}
            />
            <Card.Body>
              <Card.Title style={{ color: "#162552", fontSize: 18 }}>
                Paket Medical Check - up Gold
              </Card.Title>
              <Card.Text style={{ color: "#464646", fontWeight: "500" }}>
                Harga mulai dari
              </Card.Text>
              <Card.Text
                className="text-decoration-line-through"
                style={{ color: "#464646", fontWeight: "500" }}
              >
                Rp. 364.091
              </Card.Text>
              <Card.Text style={{ color: "#162552", fontWeight: "700" }}>
                Rp. 364.091
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className="rounded-3 shadow-lg" style={{ width: "12rem" }}>
            <Card.Img
              variant="top"
              src={Example_1}
              style={{ resizeMode: "cover" }}
            />
            <Card.Body>
              <Card.Title style={{ color: "#162552", fontSize: 18 }}>
                Paket Screening Diabetes Melitus
              </Card.Title>
              <Card.Text style={{ color: "#464646", fontWeight: "500" }}>
                Harga mulai dari
              </Card.Text>
              <Card.Text
                className="text-decoration-line-through"
                style={{ color: "#464646", fontWeight: "500" }}
              >
                Rp. 364.091
              </Card.Text>
              <Card.Text style={{ color: "#162552", fontWeight: "700" }}>
                Rp. 364.091
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className="rounded-3 shadow-lg" style={{ width: "12rem" }}>
            <Card.Img
              variant="top"
              src={Example_2}
              style={{ resizeMode: "cover" }}
            />
            <Card.Body>
              <Card.Title style={{ color: "#162552", fontSize: 18 }}>
                Paket Resiko Jantung
              </Card.Title>
              <Card.Text style={{ color: "#464646", fontWeight: "500" }}>
                Harga mulai dari
              </Card.Text>
              <Card.Text
                className="text-decoration-line-through"
                style={{ color: "#464646", fontWeight: "500" }}
              >
                Rp. 364.091
              </Card.Text>
              <Card.Text style={{ color: "#162552", fontWeight: "700" }}>
                Rp. 364.091
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className="rounded-3 shadow-lg" style={{ width: "12rem" }}>
            <Card.Img
              variant="top"
              src={Example_3}
              style={{ resize: "cover" }}
            />
            <Card.Body>
              <Card.Title style={{ color: "#162552", fontSize: 18 }}>
                Paket TORCH
              </Card.Title>
              <Card.Text style={{ color: "#464646", fontWeight: "500" }}>
                Harga mulai dari
              </Card.Text>
              <Card.Text
                className="text-decoration-line-through"
                style={{ color: "#464646", fontWeight: "500" }}
              >
                Rp. 364.091
              </Card.Text>
              <Card.Text style={{ color: "#162552", fontWeight: "700" }}>
                Rp. 364.091
              </Card.Text>
            </Card.Body>
          </Card>
        </div>
      </div>
      <div className="container" style={{ marginTop: 100, marginBottom: 100 }}>
        <div className="row justify-content-between">
          <div className="col-sm-5">
            <img src={Banner_1} alt="" />
          </div>
          <div className="col-sm-5">
            <img src={Banner_2} alt="" />
          </div>
        </div>
      </div>
      <div
        className="row py-5"
        style={{
          marginTop: 100,
          marginBottom: 100,
          backgroundColor: "#F6FBFF",
        }}
      >
        <div className="text-center">
          <h3 style={{ color: "#162552" }}>
            Mitra Laboratorium Terbaik di Indonesia
          </h3>
          <h5 style={{ color: "#162552", marginTop: 30 }}>
            Triasse tersedia di 44 cabang seluruh indonesia, dengan sebaran di
            16 kota besaryang bisa di pesan secara
            <p className="fst-italic">online</p>
          </h5>
        </div>
        <div className="row justify-content-lg-center">
          <div className="col-sm-2 text-center">
            <img src={Prodia} alt="" />
          </div>
          <div className="col-sm-2 text-center">
            <img src={Prahita} alt="" />
          </div>
          <div className="col-sm-2 text-center">
            <img src={KimiaFarma} alt="" />
          </div>
          <div className="col-sm-2 text-center">
            <img src={Biotest} alt="" />
          </div>
          <div className="col-sm-2 text-center">
            <img src={GSLab} alt="" />
          </div>
        </div>
      </div>
      <div className="row py-5">
        <div className="text-center" style={{ marginTop: 100 }}>
          <h3 style={{ color: "#162552" }}>Lorem Ipsum is simply dummy text</h3>
          <h5 style={{ color: "#162552", marginTop: 30 }}>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's
          </h5>
        </div>
        <div className="row justify-content-center mt-5 ">
          <div className="col-10">
            <div className="row">
              <div className="col-sm-3">
                <div className="row">
                  <img src={ImagePost_1} alt="" />
                  <p className="fw-bold mt-3 fs-4" style={{ color: "#0E1E4C" }}>
                    Rapid Test Covid-19 sekarang, Bayar dengan 4 kali
                  </p>
                  <p style={{ color: "#949494" }}>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s,
                  </p>
                </div>
              </div>
              <div className="col-sm-3">
                <div className="row">
                  <img src={ImagePost_1} alt="" />
                  <p className="fw-bold mt-3 fs-4" style={{ color: "#0E1E4C" }}>
                    Apa itu Meningitis ?Semua yang ada perlu tau
                  </p>
                  <p style={{ color: "#949494" }}>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s,
                  </p>
                </div>
              </div>
              <div className="col-sm-3">
                <div className="row">
                  <img src={ImagePost_1} alt="" />
                  <p className="fw-bold mt-3 fs-4" style={{ color: "#0E1E4C" }}>
                    Telat Haid : Gejala, Penyebab, dan Cara mengatasi
                  </p>
                  <p style={{ color: "#949494" }}>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s,
                  </p>
                </div>
              </div>
              <div className="col-sm-3">
                <div className="row">
                  <img src={ImagePost_1} alt="" />
                  <p className="fw-bold mt-3 fs-4" style={{ color: "#0E1E4C" }}>
                    Ingin Sukses ? ketahui 6 tips Malam Pertama
                  </p>
                  <p style={{ color: "#949494" }}>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s,
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row py-5">
        <div className="text-center" style={{ marginBottom: 100 }}>
          <h2 style={{ color: "#162552" }}>Media Coverage</h2>
        </div>
        <div className="row justify-content-center">
          <div className="col-sm-8 ">
            <div className="row justify-content-between mb-5">
              <div className="col-sm-1  text-center">
                <img src={Media_1} alt="" height="50" />
              </div>
              <div className="col-sm-1  text-center">
                <img src={Media_2} alt="" height="50" />
              </div>
              <div className="col-sm-1  text-center">
                <img src={Media_3} alt="" height="50" />
              </div>
              <div className="col-sm-1  text-center">
                <img src={Media_4} alt="" height="50" />
              </div>
              <div className="col-sm-1  text-center">
                <img src={Media_5} alt="" height="50" />
              </div>
              <div className="col-sm-1  text-center">
                <img src={Media_6} alt="" height="50" />
              </div>
              <div className="col-sm-1  text-center">
                <img src={Media_7} alt="" height="50" />
              </div>
              <div className="col-sm-1  text-center">
                <img src={Media_8} alt="" height="50" />
              </div>
              <div className="col-sm-1  text-center">
                <img src={Media_9} alt="" height="50" />
              </div>
            </div>
            <div className="row justify-content-center">
              <div className="col-sm-1  text-center">
                <img src={Media_10} alt="" height="50" />
              </div>
              <div className="col-sm-1  text-center">
                <img src={Media_11} alt="" height="50" />
              </div>
              <div className="col-sm-1  text-center">
                <img src={Media_12} alt="" height="50" />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row justify-content-center my-5">
        <div className="col-sm-10">
          <hr />
        </div>
      </div>
      <div className="row justify-content-center">
        <div className="col-sm-8">
          <div className="row">
            <h5 className="fw-bold">Toko Furniture terpercaya di indonesia</h5>
            <p className="fs-5" style={{ color: "#464646" }}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged. It was
              popularised in the 1960s with the release of Letr
            </p>
          </div>
          <div className="row">
            <h5 className="fw-bold">
              Belanja Meubel Custom terlengkap di Bramble Furniture
            </h5>
            <p className="fs-5" style={{ color: "#464646" }}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged. It was
              popularised in the 1960s with the release of Letr
            </p>
          </div>
          <div className="row">
            <h5 className="fw-bold">
              Situ Jual Beli Furniture Berkualitas premium
            </h5>
            <p className="fs-5" style={{ color: "#464646" }}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged. It was
              popularised in the 1960s with the release of Letr
            </p>
          </div>
          <div className="row">
            <h5 className="fw-bold">
              Gratis ongkir Furniture Seluruh Jabodetabek
            </h5>
            <p className="fs-5" style={{ color: "#464646" }}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged. It was
              popularised in the 1960s with the release of Letr
            </p>
          </div>
          <div className="row">
            <h5 className="fw-bold">Toko Furniture terpercaya di indonesia</h5>
            <p className="fs-5" style={{ color: "#464646" }}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged. It was
              popularised in the 1960s with the release of Letr
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};
